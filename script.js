var gameModel;

function renderTable(gameModel, tableId) {
  var table = document.getElementById(tableId);
  var tableCells = table.getElementsByTagName("td");
  var tableRows = table.getElementsByTagName("tr");
  var quantityOfLines = table.getElementsByTagName("tr").length;
  var quantityOfCells = tableRows[0].getElementsByTagName("td").length;
  var counter = 0;
  tableArray = [];
  for (var i = 0; i < quantityOfLines; i++) {
    tableArray[i] = [];
    for (var k = 0; k < quantityOfCells; k++) {
      tableArray[i][k] = tableCells[counter];
      counter++;
    }
  }

  for (var i = 0; i < quantityOfLines; i++) {
    for (var k = 0; k < quantityOfCells; k++) {
      var cell = gameModel[i][k];
      if (cell.isFilled)
        tableArray[i][k].setAttribute("class", $`{cell.color}-td`);
      else 
      tableArray[i][k].setAttribute("class", "opaque");
    }
   
      }
    }

var ar = [
  [0, 0, 0, 0],
  [1, 1, 1, 1],
  [0, 0, 0, 0],
  [0, 0, 0, 0],
];
renderTable(ar, "next");
